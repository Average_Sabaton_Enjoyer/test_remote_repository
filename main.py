from PyQt5 import QtWidgets
import calculate

from template import Ui_Form


class MainWindow(Ui_Form):

    def __init__(self, Form):
        """
          Модификация шаблона графического приложения с прикрученной
          логикой кнопок
        """
        super().__init__()
        self.setupUi(Form)
        self.list_of_button = [
            self.x11,
            self.x12,
            self.x21,
            self.x22,
            self.y11,
            self.y12,
            self.y21,
            self.y22,
        ]
        self.Calculate_distance.clicked.connect(self.on_click)

    def on_click(self):
        """
        клик на "посчитать дистанцию"

        при нажатии на кнопку считывает данные с полей ввода, считает
        необходимые дистанции и рендерит их в поле для вывода ответа

        :return: none
        """

        dict_position = [i.value() for i in self.list_of_button]
        print(dict_position)

        self.distance_bet_cent.setText(str(calculate.get_distance_between_center(*dict_position)))
        self.distance_betw_edges.setText(str(calculate.get_distance_between_squares_point(*dict_position)))

        # TODO: рендер прямоугольников на поле
        # TODO: скрол графического окна


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = MainWindow(Form)
    Form.show()
    sys.exit(app.exec_())
